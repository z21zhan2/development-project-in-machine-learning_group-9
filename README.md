# Development project in Machine Learning_Group 9

Xiayue Shen

Xiran Zhang

Zuoyu Zhang

# Introduction
This is the Development project in course 'Introduction to the theory and practice of Machine Learning' of group 9's. The objective of this project is to apply binary classification on two datasets (Banknote Authentication Dataset & Chronic Kidney Disease). In terms of machine learning model, we've chosen the following candidates according to what we have learned in class:

1. Logistic regression
2. Decision tree
3. Multilayer perceptron(MLP)
4. SVM
5. Bayesian classification
